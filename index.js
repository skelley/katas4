/* 
Hint: when displaying an array, use JSON.stringify() to 'prettify' the output. For example, lets say you use document.createElement() and store it into a variable called newElement.

To display lotrCitiesArray on this element, you can call:
newElement.textContent = JSON.stringify(lotrCitiesArray);
*/

/* 
1.  Display an array from the cities in gotCitiesCSV.
2. Display an array of words from the sentence in bestThing.
3. Display a string separated by semi-colons instead of commas from gotCitiesCSV.
4. Display a CSV (comma-separated) string from the lotrCitiesArray.
5. Display the first 5 cities in lotrCitiesArray.
6. Display the last 5 cities in lotrCitiesArray.
7. Display the 3rd to 5th city in lotrCitiesArray.
8. Using Array.prototype.splice(), remove "Rohan" from lotrCitiesArray.
9. Using Array.prototype.splice(), remove all cities after "Dead Marshes" in lotrCitiesArray.
10. Using Array.prototype.splice(), add "Rohan" back to lotrCitiesArray right after "Gondor".
11. Using Array.prototype.splice(), rename "Dead Marshes" to "Deadest Marshes" in lotrCitiesArray.
12. Using String.prototype.slice(), display the first 14 characters from bestThing.
13. Using String.prototype.slice(), display the last 12 characters from bestThing.
14.Using String.prototype.slice(), display characters between the 23rd and 38th position of bestThing (i.e. "boolean is even").
15.Repeat #13 using String.prototype.substring() instead of String.prototype.slice().
16.Repeat #14 using String.prototype.substr() instead of String.prototype.slice().
17.Find and display the index of "only" in bestThing.
18.Find and display the index of the last word in bestThing.
19.Find and display all cities from gotCitiesCSV that use double vowels ("aa", "ee", …).
20.Find and display all cities from lotrCitiesArray that end with "or".
21.Find and display all the words in bestThing that start with a "b".
22.Display "Yes" or "No" if lotrCitiesArray includes "Mirkwood".
23.Display "Yes" or "No" if lotrCitiesArray includes "Hollywood".
24.Display the index of "Mirkwood" in lotrCitiesArray.
25.Find and display the first city in lotrCitiesArray that has more than one word.
26.Reverse the order in lotrCitiesArray.
27.Sort lotrCitiesArray alphabetically.
28.Sort lotrCitiesArray by the number of characters in each city (i.e., shortest city names go first).
29.Using Array.prototype.pop(), remove the last city from lotrCitiesArray.
30.Using Array.prototype.push(), add back the city from lotrCitiesArray that was removed in #29 to the back of the array.
31.Using Array.prototype.shift(), remove the first city from lotrCitiesArray.
32.Using Array.prototype.unshift(), add back the city from lotrCitiesArray that was removed in #31 to the front of the array.*/ 


const theDiv= document.getElementById("theDiv")
const gotCitiesCSV = "King's Landing, Braavos, Volantis, Old Valyria, Free Cities, Qarth, Meereen";
const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";
const vowelMan = ["aa", "ee", "ii", "oo", "uu"]

//makes H2 header
function makeHeader(header) {
    let div = document.createElement("H2")
    div.textContent = header
    document.getElementById("theDiv").appendChild(div)
}

//makes prints answer to page
function printAnswer(value) {
    div = document.createElement("div")
    div.textContent = value
    document.getElementById("theDiv").appendChild(div)
}
    
    
    

//Display an array from the cities in gotCitiesCSV.
makeHeader("Number 1")
printAnswer(gotCitiesCSV.split(","));

//2.Display an array of words from the sentence in bestThing.
makeHeader("Number 2")
printAnswer(bestThing.split(" "));


//3.Display a string separated 
//by semi-colons instead of commas from gotCitiesCSV.
makeHeader("Number 3")
printAnswer(gotCitiesCSV.replace("," , ";"))

//4.Display  a CSV (comma-separated) string from the lotrCitiesArray.
makeHeader("Number 4")
printAnswer(lotrCitiesArray.join(", "))

//5.Display the first 5 cities in lotrCitiesArray.
makeHeader("Number 5")
printAnswer(lotrCitiesArray.slice(0,5))

//6.Display the last 5 cities in lotrCitiesArray.
makeHeader("Number 6")
printAnswer(lotrCitiesArray.slice(-5))


//7.Display the 3rd to 5th city in lotrCitiesArray.
makeHeader("Number 7")
printAnswer(lotrCitiesArray.slice(3,6))

//8.
makeHeader("Number 8")
lotrCitiesArray.splice(2,1)
printAnswer(lotrCitiesArray)


makeHeader("Number 9")
lotrCitiesArray.splice(-2,2)
printAnswer(lotrCitiesArray)


makeHeader("Number 10")
lotrCitiesArray.splice(2,0, "Rohan")
printAnswer(lotrCitiesArray)


//11.Using Array.prototype.splice(), rename "Dead Marshes" to "Deadest Marshes" in lotrCitiesArray.
makeHeader("Number 11")
lotrCitiesArray.splice(5,1, "Deadest Marshes")
printAnswer(lotrCitiesArray)

//Using String.prototype.slice(), display the first 14 characters from bestThing.
makeHeader("Number 12")
printAnswer(bestThing.slice(0,15))

//13.Using String.prototype.slice(), display the last 12 characters from bestThing.
makeHeader("Number 13")
printAnswer(bestThing.slice(-12))


//14.Using String.prototype.slice(), display characters between the 23rd and 38th
// position of bestThing (i.e. "boolean is even").
makeHeader("Number 14")
printAnswer(bestThing.slice(24,39))



//15.Repeat #13 using String.prototype.substring() instead of String.prototype.slice().
makeHeader("Number 15")
printAnswer(bestThing.substring(69))



//16.Repeat #14 using String.prototype.substr() instead of String.prototype.slice().
makeHeader("Number 16")
printAnswer(bestThing.substring(24,34))


//17.Find and display the index of "only" in bestThing.
makeHeader("Number 17")
printAnswer(bestThing.search("only"))

//18.Find and display the index of the last word in bestThing
makeHeader("Number 18")

printAnswer(bestThing.search("bit"))
printAnswer(lotrCitiesArray)

//19.Find and display all cities from gotCitiesCSV that use double vowels ("aa", "ee", …).
makeHeader("Number 19")
let splitnerCell = gotCitiesCSV.split(',')
rayMan = []
for(i=0; i < splitnerCell.length; i++){
    for(k=0; k<splitnerCell[i].length-1; k++){
        for(j=0; j<vowelMan.length; j++){

            if(splitnerCell[i][k] + splitnerCell[i][k+1] == vowelMan[j]){
                if(!rayMan.includes(splitnerCell[i]))
                    rayMan.push(splitnerCell[i]) 
                           
        }
        
            }
    }
}
printAnswer(JSON.stringify(rayMan))

//20.Find and display all cities from lotrCitiesArray that end with "or".
makeHeader("Number 20")
matrix=[]
for(i=0; i < lotrCitiesArray.length; i++){
    const cur_val = lotrCitiesArray[i]
    if(cur_val.slice(cur_val.length-2, cur_val.length) == "or"){
        matrix.push(cur_val)
    }
}
printAnswer(JSON.stringify(matrix))

//21.Find and display all the words in bestThing that start with a "b".
makeHeader("Number 21")
let binderCell = bestThing.split(' ')
matrix2=[]
for(i=0; i < binderCell.length; i++){
    const result = binderCell[i]
    if(result[0] == "b" ){
        matrix2.push(result)
    }
}

printAnswer(JSON.stringify(matrix2))

//22.Display "Yes" or "No" if lotrCitiesArray includes "Mirkwood".
makeHeader("Number 22")
matrix3=[]

for(i=0; i < lotrCitiesArray.length; i++){
    const result = "Yes" 
    const resultno = "no"
    const mari = lotrCitiesArray[i]
    if(mari == "Mirkwood"){
        matrix3.push(result)
    } else {
        matrix3.push(resultno)
    }
}
printAnswer(JSON.stringify(matrix3))

//23.Display "Yes" or "No" if lotrCitiesArray includes "Hollywood".
makeHeader("Number 23")
Matrix4=[]

for(i=0; i < lotrCitiesArray.length; i++){
    const result = "Yes" 
    const resultt = "no"
    const mari = lotrCitiesArray[i]
    if(mari == "Hollywood"){
        Matrix4.push(result)
    } else {
        Matrix4.push(resultt)
    }
}

printAnswer(JSON.stringify(Matrix4))

//24.Display the index of "Mirkwood" in lotrCitiesArray.
makeHeader("Number 24")
const index =  lotrCitiesArray.findIndex( city => city === "Mirkwood");
printAnswer(index)


//25.Find and display the first city in lotrCitiesArray that has more than one word.
makeHeader("Number 25")
printAnswer(lotrCitiesArray[5])

//26.Reverse the order in lotrCitiesArray.
makeHeader("Number 26")
printAnswer(lotrCitiesArray.reverse(lotrCitiesArray))

//27.Sort lotrCitiesArray alphabetically.
makeHeader("Number 27")
printAnswer(lotrCitiesArray.sort())

//28.Sort lotrCitiesArray by the number of 
//characters in each city (i.e., shortest city names go first).
makeHeader("Number 28")
printAnswer(
lotrCitiesArray.sort(function(a, b){
    return b.length - a.length;
}).reverse(lotrCitiesArray))

//29.Using Array.prototype.pop(), remove the last city from lotrCitiesArray.
makeHeader("Number 29")
lotrCitiesArray.reverse().pop()
printAnswer(lotrCitiesArray)


//30.Using Array.prototype.push(), add back the city from 
//lotrCitiesArray that was removed in #29 to the back of the array.
makeHeader("Number 30")
lotrCitiesArray.push("Rohan")
printAnswer(lotrCitiesArray)

//31.Using Array.prototype.shift(), remove the first city from lotrCitiesArray.
makeHeader("Number 31")

lotrCitiesArray.shift()
printAnswer(lotrCitiesArray)

//32.Using Array.prototype.unshift(), add back the city from lotrCitiesArray 
//that was removed in #31 to the front of the array.
makeHeader("Number 32")

lotrCitiesArray.unshift("Deadest Marshes")
printAnswer(lotrCitiesArray)

/*
const bestThingArray = bestThing.split(" ")
printAnswer(bestThingArray);*/










